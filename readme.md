## Purpose

This repository contains shared settings to be used by developers using IntelliJ. It ensures (amounst other things) that IntelliJ generates code to teh Mirada standard

## How to Use
- In Intellij go to File > Settings > Tools > Settings Repository
- Click + on the read only sources
- Add http://bitbucket/scm/inf/intellij-settings-repository.git as a source

## How to update these settings
Easiest option is probably to make the changes 